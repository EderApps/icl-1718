package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import ast.ASTDecl.Declaration;
import ast.Param;
import compiler.CodeBlock.StackFrame;
import types.BoolType;
import types.FuncType;
import types.IType;
import types.IntType;
import types.RefType;
import util.Address;
import util.IEnvironment;

public class CodeBlock {
	
	public static class StackFrame {
		
		int id;
		StackFrame up;		
		//ArrayList<IType> locs;
		public ArrayList<Declaration> locs;
		int env;
		LinkedList<Param> params;
		
		public int getID() {
			return id;
		}
		
		public int getEnv() {
			return env;
		}
		
		public StackFrame getUp() {
			return up;
		}
		
		StackFrame(int id, StackFrame up, ArrayList<Declaration> decls, int env, LinkedList<Param> params){
			this.id = id;
			this.up = up;
			this.locs = decls;
			this.env = env;
			this.params = params;
		}
		
		void dump() throws FileNotFoundException {
			
					/*
					 * 	frame_id.j
					 * 
						.class frame_id
						.super java/lang/Object
						.field public SL Lancestor_frame_id; 
						.field public loc_00 type;
						.field public loc_01 type;
						..
						.field public loc_n type;
						.end method 
					 */
			PrintStream out = new PrintStream(new FileOutputStream("frame_"+id+".j"));
			out.println(".class frame_"+id);
			out.println(".super java/lang/Object");
			out.println("");
			if (up != null) {
				out.println(".field public SL Lframe_"+up.getID()+";");
			}
			String num = "00";
			int i = 0;
			if(locs !=null) {
			for(Declaration decl: locs) {
				if (i > 9) {
					num = ""+i;
				}else {
					num = "0"+i;
				}
				System.out.println("frame "+decl.def.getType());
				out.println(".field public loc_"+num+" "+toJasmin(decl.def.getType()));
				i++;
			}
			}
			
			i = 0;
			if(params !=null) {
			for(Param p: params) {
				if (i > 9) {
					num = ""+i;
				}else {
					num = "0"+i;
				}
				out.println(".field public loc_"+num+" "+toJasmin(p.getParamType()));
				i++;
			}
			}
			out.println("");

			out.println(".method public <init>()V");
			out.println("aload_0");
			out.println("invokenonvirtual java/lang/Object/<init>()V");
			out.println("return");
			out.println(".end method");
			
	

			
				// escrever num file a parte
				// id, anterior, arraylist com tipos de locs
			
		}
	}
	
	
	public static class FunInterface {
		int id;
		IType type;
		
		public int getID() {
			return id;
		}
		
		public IType getType() {
			return type;
		}
		
		public FunInterface(int id, IType type) {
			this.id = id;
			this.type = type;
			try {
				dump();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		void dump() throws FileNotFoundException {
			
			
			PrintStream out = new PrintStream(new FileOutputStream("Funtype_"+id+".j"));
			out.println(".interface public Funtype_"+id);
			out.println(".super java/lang/Object");
			
			out.println(".method abstract public apply("+toJasmin(((FuncType)type).getRetType())+")I");
			out.println(".end method");
			
		}		
	}
	
	
	public static class RefClass {
		
		int id;
		String title;
		IType mytype;
		IType subtype;

		RefClass(int id, IType mytype, IType subtype){
			this.id = id;
			title = "ref_"+id;
			this.mytype = mytype;
			this.subtype = subtype;
		}
		
		public int getId() {
			return id;
		}
		
		public String getTitle() {
			return title;
		}
		
		void dump() throws FileNotFoundException {
			
			
			PrintStream out = new PrintStream(new FileOutputStream(mytype+".j"));
			out.println(".source "+mytype+".j");
			out.println(".class "+mytype);
			out.println(".super java/lang/Object");
			
			out.println("");
			/*if(toJasmin(type).substring(0, 1).equals("L")) {
				int id1 = id+1;
				out.println(".field public value "+"Lref_"+id1+";");
			}else {*/
				out.println(".field public value "+toJasmin(subtype));
			//}
			out.println("");

			out.println(".method public <init>()V");
			out.println("aload_0");
			out.println("invokespecial java/lang/Object/<init>()V");
			out.println("return");
			out.println(".end method");
			
		}
	}
	
	
	public static class ClosureClass {
		
		FunInterface fi;
		int closureID;
		StackFrame frame;
		
		public int getID() {
			return closureID;
		}
		
		public StackFrame getSLFrame() {
			return frame;
		}
		
		public FunInterface getInterface() {
			return fi;
		}
		
		 LinkedList<Param> params;
		 IEnvironment<Address> newenv;
		
		public ClosureClass(FunInterface fi, int closureID, LinkedList<Param> params, IEnvironment<Address> newenv) {
			this.fi = fi;
			this.closureID = closureID;
			this.frame = currFrame;
			this.params = params;
			this.newenv = newenv;
			try {
				dump();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		void dump() throws FileNotFoundException {
			
			
			PrintStream out = new PrintStream(new FileOutputStream("Closure_"+closureID+".j"));
			out.println(".class public Closure_"+closureID);
			out.println(".super java/lang/Object");
			out.println(".implements Funtype_"+fi.getID());
			/*if(toJasmin(type).substring(0, 1).equals("L")) {
				int id1 = id+1;
				out.println(".field public value "+"Lref_"+id1+";");
			}else {*/
				out.println(".field public SL Lframe_"+currFrame.getID()+";");
			//}
				int childID = currFrame.getID();
			out.println(".method public apply(I)I");
			out.println(".limit locals 11\n" + 
						".limit stack 256");

			StackFrame f = new StackFrame(++currID, currFrame, null, getEnvLevel(),params);
			frames.add(f);
			currFrame = f;

			out.println();
			
			int currSP = 0;
			
			out.println("new frame_"+currFrame.getID());
			out.println("dup");
			out.println("invokespecial frame_"+currFrame.getID()+"/<init>()V");
			out.println("dup");
			out.println("aload "+currSP);
			out.println("getfield Closure_"+closureID+"/SL Lframe_"+childID+";");
			out.println("putfield frame_"+currFrame.getID()+"/SL Lframe_"+childID+";");
			out.println("dup");
			out.println("iload "+(++currSP));
			
			//THIS PART IS WRONG. TESTING PURPOSES
			
			out.println("putfield frame_3/loc_00 I\n" + 
					"astore 2\n" + 
					"aload 2\n" + 
					"checkcast frame_3\n" + 
					"getfield frame_3/loc_00 I\n"
					+ "sipush 1\n" + 
					"iadd\n" + 
					"ireturn");
			out.println(".end method");

			out.println(".method public <init>()V");
			out.println("aload_0");
			out.println("invokenonvirtual java/lang/Object/<init>()V");
			out.println("return");
			out.println(".end method");
			
		}
		
	}
	
	public int getID() {
		return (++currID);
	}
	
	public int getRefID() {
		return (++refID);
	}
	
	public int getCurrFunID() {
		return (++currFunID);
	}
	
	public StackFrame getCurrFrame() {
		return currFrame;
	}
	
	public RefClass getCurrRef() {
		return currRef;
	}
	
	public void incEnvLevel() {
		envLevel++;
	}
	
	public void decEnvLevel() {
		envLevel--;
	}
	
	public void setCurrFrame (StackFrame f) {
		currFrame = f;
	}
	
	public void setCurrRef (RefClass r) {
		currRef = r;
	}
	
	public static int getEnvLevel() {
		return envLevel;
	}
	
	public FunInterface createOrReuse(IType type) {
		FunInterface fun = null;
		if (funs.get(type) == null) {
			fun = new FunInterface(getCurrFunID(), type);
			funs.put(type, fun);
		}else {
			fun = funs.get(type);
		}
		
		return fun;
	}
	
	public static String toJasmin(IType type) {
		if (type.toString().equals("int") || type.toString().equals("bool")) {
			return "I";
		}else {
			System.out.println("gg "+(type));
			return "L"+type+";";
		}
	}
	
	public void new_frame(ArrayList<Declaration> decls) {
		StackFrame f = new StackFrame(getID(), currFrame, decls, getEnvLevel(), null);
		System.out.println(getEnvLevel());
		frames.add(f);
		currFrame = f;
	}
	
	public void new_ref(IType type) throws FileNotFoundException {
		RefClass r = new RefClass(getRefID(), type, ((RefType)type).getType());
		
		if(refs.get(type) == null) {
			refs.put(type, r);
		}
		for(Entry<IType,RefClass> e: refs.entrySet()) {
			System.out.println(e.getKey()+" ok it's "+e.getValue());;
		}
		currRef = r;
		r.dump();
	}
	
	static Map<IType,RefClass> refs;
	static Map<IType,FunInterface> funs;

	static int envLevel = 0;
	static int currID = 0;
	int refID = 0;
	int currFunID = 0;
	static ArrayList<String> code;
	
	LinkedList<StackFrame> currFrames;
	
	RefClass currRef;
	static StackFrame currFrame;
	static ArrayList<StackFrame> frames;
	public static final LabelFactory labelFactory = new LabelFactory();
	
	public CodeBlock() {
		frames = new ArrayList<StackFrame>(100);
		code = new ArrayList<String>(100);
		currFrames = new LinkedList<StackFrame>();
		currFrame = null;
		currRef = null;
		refs = new HashMap<IType,RefClass>();
		funs = new HashMap<IType,FunInterface>();
	}

	public void emit_push(int n) {
		code.add("sipush "+n);
	}
	
	public void emit_bool(boolean n) {
		if(n)
			code.add("iconst_1");
		else
			code.add("iconst_0");
	}

	public void emit_add() {
		code.add("iadd");
	}

	public void emit_mul() {
		code.add("imul");
	}

	public void emit_div() {
		code.add("idiv");
	}

	public void emit_sub() {
		code.add("isub");
	}

	void dumpHeader(PrintStream out) {
		out.println(".class public Demo");
		out.println(".super java/lang/Object");
		out.println("");
		out.println(";");
		out.println("; standard initializer");
		out.println(".method public <init>()V");
		out.println("   aload_0");
		out.println("   invokenonvirtual java/lang/Object/<init>()V");
		out.println("   return");
		out.println(".end method");
		out.println("");
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println("       ; set limits used by this method");
		out.println("       .limit locals 10");
		out.println("       .limit stack 256");
		out.println("");
		out.println("       ; setup local variables:");
		out.println("");
		out.println("       ;    1 - the PrintStream object held in java.lang.out");
		out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		out.println("");
		out.println("       ; place your bytecodes here");
		out.println("       ; START");
		out.println("");
	}

	void dumpFooter(PrintStream out) {
		out.println("       ; END");
		out.println("");
		out.println("");		
		out.println("       ; convert to String;");
		out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("       ; call println ");
		out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("");		
		out.println("       return");
		out.println("");		
		out.println(".end method");
	}

	void dumpCode(PrintStream out) {
		for( String s : code )
			out.println("       "+s);
	}
	
	public void dump(String filename) throws FileNotFoundException {
		PrintStream out = new PrintStream(new FileOutputStream(filename));
		dumpHeader(out);
		dumpCode(out);
		dumpFooter(out);
		dumpFrames();
		
	}

	private void dumpFrames() throws FileNotFoundException {
		for(StackFrame frame: frames)
			frame.dump();
	}

	public void emit_ifeq(String label) {
		code.add("ifeq "+label);
	}
	
	public void emit_jump(String label) {
		code.add("goto "+label);
	}
	
	public void emit_anchor(String label) {
		code.add(label + ":");
	}

	public void emit_ifcmpne(String label) {
		code.add("if_icmpne "+label);		
	}
	
	public void emit_ifcmpeq(String label) {
		code.add("if_icmpeq "+label);		
	}
	
	public void emit_ineg() {
		code.add("ineg");		
	}
	
	public void emit_ificmplt(String label) {
		code.add("if_icmplt "+label);		
	}
	
	public void emit_ificmpgt(String label) {
		code.add("if_icmpgt "+label);		
	}
	
	public void emit_ificmpge(String label) {
		code.add("if_icmpge "+label);		
	}
	
	public void emit_ificmple(String label) {
		code.add("if_icmple "+label);		
	}
	
	public void emit_dup() {
		code.add("dup");		
	}
	
	public void emit_aload(int val) {
		code.add("aload "+val);		
	}
	
	public void emit_frameid(int id) {
		code.add("new frame_"+id);		
	}
	
	public void emit_invokespecialframe(int id) {
		code.add("invokespecial frame_"+id+"/<init>()V");		
	}
	
	public void emit_putfieldframe(int currid, int previd) {
		code.add("putfield frame_"+currid+"/SL Lframe_"+previd+";");		
	}
	
	public void emit_putfieldloc(int currid, String num, Declaration decl) {
		code.add("putfield frame_"+currid+"/loc_"+num+" "+toJasmin(decl.def.getType()));		
	}
	
	public void emit_getfieldframe(int currid, int previd) {
		code.add("getfield frame_"+currid+"/SL Lframe_"+previd+";");		
	}
	
	public void emit_getfieldloc(int currid, String num, Declaration decl) {
		code.add("getfield frame_"+currid+"/loc_"+num+" "+toJasmin(decl.def.getType()));		
	}
	
	public void emit_astore(int val) {
		code.add("astore "+val);		
	}
	
	public void emit_aconstnull() {
		code.add("aconst_null");		
	}
	
	public void emit_checkcast(int id) {
		code.add("checkcast frame_"+id);		
	}
	
	public void emit_pop() {
		code.add("pop");
	}
	
	public void emit_newref(IType t) {
		code.add("new "+t);
	}
	
	public void emit_invokespecialRef(IType t) {
		code.add("invokespecial "+t+"/<init>()V");
	}
	
	public void emit_putfieldref(IType ty) {
		System.out.println(toJasmin(ty));
		
		code.add("putfield "+refs.get(ty).mytype+"/value "+toJasmin(refs.get(ty).subtype));
		
	}
	
	IType lastType;
	
	public void emit_getfieldref(IType ty) {
		
		if (refs.get(ty) == null) {
			code.add("getfield "+refs.get(lastType).mytype+"/value "+toJasmin(refs.get(lastType).subtype));
			lastType = refs.get(lastType).subtype;
		}else {
			code.add("getfield "+refs.get(ty).mytype+"/value "+toJasmin(refs.get(ty).subtype));
			lastType = refs.get(ty).subtype;
		}
	}

	public void emit_putfieldassign(IType left, IType ty) {
		System.out.println("eheh "+ty+" "+refs.get(ty));

		code.add("putfield "+refs.get(left).mytype+"/value "+toJasmin(refs.get(left).subtype));
	}

	public void emit_invokespecialapply(IType type, int i) {
		code.add("invokeinterface Funtype_"+funs.get(type).getID()+"/apply(I)I "+i);
	}

	public ClosureClass createClosure(FunInterface closureSignature, LinkedList<Param> params, IEnvironment<Address> newenv) {
		return new ClosureClass(closureSignature,++closureID, params, newenv);
	}

	public int closureID = 0;
	
	public void emit_newClosure(ClosureClass closure) {
		code.add("new Closure_"+closure.getID());
		code.add("dup");
		code.add("invokespecial Closure_"+closure.getID()+"/<init>()V");		
		code.add("dup");
		code.add("aload 0");
		code.add("putfield Closure_"+closure.getID()+"/SL Lframe_"+closure.getSLFrame().getID()+";");
	}

	
	
	
}