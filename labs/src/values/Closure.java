package values;

import java.util.LinkedList;

import ast.ASTNode;
import ast.Param;
import util.IEnvironment;

public class Closure implements IValue {
	
	private int value;

	public LinkedList<Param> params;
	public ASTNode body;
	public IEnvironment<IValue> env;
	
	public Closure(LinkedList<Param> params, ASTNode body, IEnvironment<IValue> env) {
		this.params = params;
		this.body = body;
		this.env = env;
	}

	public int getValue() {
		return value;
	}
	
	public LinkedList<Param> getParams(){
		return params;
	}
	
	public ASTNode getBody() {
		return body;
	}

	@Override
	public String toString() {
		return Integer.toString(value);
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Closure) {
			return this.value == ((Closure)obj).getValue();
		}
		else
			return false;
	}
	
}
