package tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

import main.Compiler;
import parser.ParseException;

public class CompilerTests {

	// This test function was designed to work with Unix like systems. 
	
	private void testCase(String expression, String result) 
			throws IOException, InterruptedException, ParseException, FileNotFoundException {
		
		Process p;
		
		p = Runtime.getRuntime().exec(new String[]{"sh", "-c", "rm *.j *.class"});
	    p.waitFor();	    

	    System.out.println("Compiling to Jasmin source code");

	    Compiler.compile(expression);
		
	    System.out.println("Compiling to Jasmin bytecode");

	    p = Runtime.getRuntime().exec(new String[]{"sh", "-c", "pwd"});
	    p.waitFor();
	    BufferedReader reader1 = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

	    StringBuffer output1 = new StringBuffer();
       String line1 = "";			
       while ((line1 = reader1.readLine())!= null) {
       		output1.append(line1 + "\n");
       }
	    System.out.println(output1.toString());
	    
		p = Runtime.getRuntime().exec(new String[]{"sh", "-c", "java -jar jasmin.jar *.j"});
	    p.waitFor();	    
	    System.out.println(p.exitValue());
	    assertTrue("Compiled to Jasmin bytecode", p.exitValue() == 0);

	    
	    BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

	    StringBuffer output = new StringBuffer();
        String line = "";			
        while ((line = reader.readLine())!= null) {
        		output.append(line + "\n");
        }
	    System.out.println(output.toString());

		p = Runtime.getRuntime().exec(new String[] {"sh","-c", "java Demo"});
	    p.waitFor();

	    reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

	    output = new StringBuffer();
        line = "";			
        while ((line = reader.readLine())!= null) {
        		output.append(line + "\n");
        }
	    System.out.println("Output: #"+output.toString()+"#");
	    
	    assertTrue(result.equals(output.toString()));
	}
	
	private void testCase(String expression, int value) throws FileNotFoundException, IOException, InterruptedException, ParseException {
		testCase(expression, value+"\n");		
	}

	@Test
	public void BasicTest() throws IOException, InterruptedException, ParseException{
		testCase("1\n", 1);
	}

	@Test
	public void testsLabClass02() throws Exception {
		testCase("1+2\n",3);
		testCase("1-2-3\n",-4);
		testCase("4*2\n",8);
		testCase("4/2/2\n",1);
		
		testCase("4 > 2\n",1);
	}
	
	@Test
    public void testsLabClass05() throws Exception {
        testCase("decl x = 1 in x+1 end\n",2);
        testCase("decl x = 1 in decl y = 2 in x+y end end\n",3);
        testCase("decl x = decl y = 2 in 2*y end in x*3 end\n",12);
        testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y end\n", 6);
        testNegativeCase("x+1\n");
        testNegativeCase("decl x = 1 in x+1 end + x\n");
        testNegativeCase("decl x = 1 in x+2 end * decl y = 1 in 2*y+x end\n");
    }
	private void testNegativeCase(String s) throws InterruptedException, ParseException, IOException {
        testCase(s, "");
    }
}





