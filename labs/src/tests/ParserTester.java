package tests;

import static org.junit.Assert.*;
import org.junit.Test;

import main.Console;
import parser.ParseException;


public class ParserTester {

	private void testCase(String expression) throws ParseException {
		assertTrue(Console.accept(expression));		
	}
	
	private void testNegativeCase(String expression) throws ParseException {
		assertFalse(Console.accept(expression));
	}
	
	@Test
	public void test01() throws Exception {
		testCase("1;;");
	}

	@Test
	public void test02ArithmeticOps() throws Exception {
		testCase("1+2;;");
		testCase("1-2-3;;");
		testCase("4*2;;");
		testCase("4/2/2;;");
	}
	
	@Test
	public void test03Error01() throws Exception {
		testNegativeCase("1++1;;");
		testNegativeCase("*2;;");
		testNegativeCase("4/+2/2;;");
	}
	
	@Test
	public void testsLabClass01() throws Exception {
		testCase("-1;;");
		testCase("-1*3;;");
		testCase("true;;");
		testCase("false;;");
		testCase("11 < 22;;");
		testCase("11 > 22;;");
		testCase("11 == 22;;");
		testCase("3*5 != 1+2 == true;;");
		testCase("1 == 2 && 3 == 4;;");
		testCase("1 == 2 || 3 == 4 && xpto ;;");
		testCase("!(1 == 2) && xpto ;;");
		testNegativeCase("< 11;;");
		testNegativeCase("11 >;;");
		testNegativeCase("<= 11;;");
		testNegativeCase("&& A;;");
	}
}









