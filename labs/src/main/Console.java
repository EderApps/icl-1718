package main;

import java.io.ByteArrayInputStream;

import ast.ASTNode;
import ast.TypeMismatchException;
import parser.ParseException;
import parser.Parser;
import parser.SimpleParser;
import parser.TokenMgrError;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.IEnvironment;
import util.MemoryImpl;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class Console {

	@SuppressWarnings("static-access")
	public static void main(String args[]) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		Parser parser = new Parser(System.in);

		while (true) {
			try {
				ASTNode n = parser.Start();
				IEnvironment<IValue> envVal = new Environment<IValue>();
				IEnvironment<IType> envType = new Environment<IType>();
				MemoryManagement m = new MemoryImpl();
				n.typecheck(envType);
				System.out.println("OK! = "+n.eval(envVal,m) );
			} catch (TokenMgrError e) {
				System.out.println("Lexical Error!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (ParseException e) {
				System.out.println("Syntax Error!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (TypeMismatchException e) {
				// TODO Auto-generated catch block
				System.out.println("Mismatch Error!");
				e.printStackTrace();
			} catch (TypingException e) {
				// TODO Auto-generated catch block
				System.out.println("Static Typing Error!");
				e.printStackTrace();
			}
		}
	}

	public static boolean accept(String s) throws ParseException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			parser.Start();
			return true;
		} catch (TokenMgrError e) {
			return false;
		} catch (ParseException e) {
			return false;
		}
	}

	public static boolean acceptCompare(String s, IValue value) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			ASTNode n = parser.Start();
			IEnvironment<IValue> envVal = new Environment<IValue>();
			MemoryManagement m = new MemoryImpl();

			return n.eval(envVal,m).equals(value);
//			return n.eval() == new IntValue(value);
		} catch (TokenMgrError e) {
			return false;
		} catch (ParseException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean acceptCompareType(String s, IType type) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			ASTNode n = parser.Start();
			IEnvironment<IType> envType = new Environment<IType>();
			return n.typecheck(envType).equals(type);
//			return n.eval() == new IntValue(value);
		} catch (TokenMgrError e) {
			return false;
		} catch (ParseException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
		}


}
