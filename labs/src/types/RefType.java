package types;

public class RefType implements IType {
	   private IType type;
	   public RefType(IType type) { this.type = type;}
	   public IType getType() { return type; }
			   
	   @Override
	   public String toString() {
		   return "ref_"+this.type.toString();
	   }
	   
	   @Override
	   public int hashCode() {
		   return this.toString().hashCode();
	   }
	   
	   
}