package types;

import java.util.LinkedList;

public class FuncType implements IType{

	IType retType;
	LinkedList<IType> parTypes;
		
	public FuncType(IType t, LinkedList<IType> types)
	{
		this.retType = t;
		this.parTypes = types;
	}	
	
	@Override
	public String toString() {
		return "fun";
	}
	
	
	public IType getRetType() {
		return retType;
	}
	
	public LinkedList<IType> getParTypes(){
		return parTypes;
	}
	
	
	@Override
	public boolean equals(Object f) {
		FuncType f1 = (FuncType) f;
		boolean broke = false;
		if(f1.getRetType().equals(retType)) {
			int i = 0;
			IType currTypeFromPar = f1.getParTypes().get(i);
			for(IType t: parTypes) {
				if(t.equals(currTypeFromPar)) {
					i++;
					if(f1.getParTypes().size() == i) {
						break;
					}else {
						currTypeFromPar = f1.getParTypes().get(i);
					}
				}else {
					broke = true;
					break;
				}
			}
			
			if(!broke) {
				return true;
			}else {
				return false;
			}
		}
		return false;
		
	}
	
}
