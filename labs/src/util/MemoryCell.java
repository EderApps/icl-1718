package util;

import values.IValue;
import values.RefValue;

public class MemoryCell implements RefValue {
	
	IValue value;
	
	MemoryCell(IValue value) {
		this.value = value;
	}
	
}