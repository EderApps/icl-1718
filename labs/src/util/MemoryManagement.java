package util;

import values.IValue;
import values.RefValue;

public interface MemoryManagement {
	 
	  RefValue create(IValue value);
	  
	  IValue get(RefValue ref);
	  
	  IValue set(RefValue ref, IValue value);
	  
	  void free(RefValue ref);
	
}