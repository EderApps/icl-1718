package util;

import util.Environment.Assoc;

public class LookupEnv<T> extends Environment<T> {
	
	public Address lookup(String id) throws UndeclaredIdentifierException {
		int jumps = 0;
		int loc = 0;
		Environment<T> current = this;

		while(current != null) {
			loc = 0;
			for(Assoc<T> assoc: current.assocs) {
				loc++;
				if( assoc.id.equals(id)) {
					return new Address(jumps, loc);
				}
			}
			jumps++;
			current = current.up;
		}
		throw new UndeclaredIdentifierException(id);
	}
	
	
}
