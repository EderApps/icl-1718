package util;

import values.IValue;
import values.RefValue;

public class MemoryImpl implements MemoryManagement {
	 
	  public RefValue create(IValue value) {
	    return new MemoryCell(value);
	  }
	
	  public IValue get(RefValue ref) {
	    return ((MemoryCell) ref).value;
	  }
	  public IValue set(RefValue ref, IValue value) {
	    return ((MemoryCell) ref).value = value;
	  }
	  
	  public void free(RefValue ref) {}
	  
	}