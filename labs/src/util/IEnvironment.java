package util;

public interface IEnvironment<T> {
	IEnvironment<T> beginScope();
	IEnvironment<T> endScope();
	void assoc(String id, T value) throws DuplicateIdentifierException;
	T find(String id) throws UndeclaredIdentifierException;
	Address lookup(String id, int i) throws UndeclaredIdentifierException;
}

