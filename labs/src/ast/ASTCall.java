package ast;

import java.util.LinkedList;

import compiler.CodeBlock;
import types.FuncType;
import types.IType;
import types.IntType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.Environment;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.Closure;
import values.IValue;
import values.IntValue;

public class ASTCall implements ASTNode {

	ASTNode e;
	LinkedList<ASTNode> l;
	IType type;
	
	public ASTCall(ASTNode e, LinkedList<ASTNode> l) {		
		this.e = e;
		this.l = l;
	}

	@Override
	public String toString() {
		return e.toString() + " + " + l.toString();
	}

	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException, UndeclaredIdentifierException  {
		
		IValue fv = e.eval(env, m);
		
		if (fv instanceof Closure) {
			Closure c = (Closure)fv;
			IEnvironment<IValue> envlocal = env.beginScope();
			int i = 0;
			for(Param p: c.getParams()) {
				envlocal.assoc(p.getParamX() , l.get(i).eval(env, m));
				i++;
			}
			IValue val = c.getBody().eval(envlocal, m);
			env = envlocal.endScope();
			return val;
		}else {
			throw new TypeMismatchException();
		}
		
		
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException, UndeclaredIdentifierException {
		IType et = e.typecheck(env);
				
		if(et instanceof FuncType) {
			
			FuncType ft = (FuncType)et;
			int i = 0;
			IType currTypeFromPar = ft.getParTypes().get(i);
			
			boolean broke = false;
			
			for(ASTNode n: l) {
				if(n.typecheck(env).equals(currTypeFromPar)) {
					i++;
					if(ft.getParTypes().size() == i) {
						break;
					}else {
						currTypeFromPar = ft.getParTypes().get(i);
					}
				}else {
					broke = true;
					break;
				}
				
			}
			
			if(broke) {
				throw new TypingException();
			}else {
				return type = et;
			}
						
		}else {
			throw new TypingException();
		}
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		e.compile(code, env);
		
		for(ASTNode n: l) {
			n.compile(code, env);
		}
		
		//invokespecial("apply")
		code.emit_invokespecialapply(type, l.size()+1);
		
	}

	@Override
	public IType getType() {
		return type;
	}
}
