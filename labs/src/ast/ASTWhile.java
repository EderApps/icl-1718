package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.Environment;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class ASTWhile implements ASTNode {

	ASTNode left, right;

	public ASTWhile(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " + " + right.toString();
	}

	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException, UndeclaredIdentifierException  {
		IValue l = left.eval(env,m);
		IValue r = null;
		while(((BoolValue)left.eval(env,m)).getValue()) {
			r = right.eval(env,m);
		}
		
		return new BoolValue(false);
		
		
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException, UndeclaredIdentifierException {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if(l == BoolType.singleton) {
			return type = BoolType.singleton;
		}else {
			throw new TypingException();
		}
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		 String labelStart, labelExit;
		
		labelStart = code.labelFactory.getLabel();
		labelExit = code.labelFactory.getLabel();
		
		code.emit_anchor(labelStart);
		
		left.compile(code, env);
		
		code.emit_ifeq(labelExit);
		
		right.compile(code, env);
		
		code.emit_pop();
		
		code.emit_jump(labelStart);
		
		code.emit_anchor(labelExit);
		
		code.emit_push(0);
		
	}
	
	IType type;

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}
}
