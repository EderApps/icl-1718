package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import types.TypingException;
import util.Address;
import util.IEnvironment;
import util.MemoryManagement;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class ASTBool implements ASTNode {

	boolean val;
	IType type;
	public ASTBool(boolean n) {
		val = n;
	}

	@Override
	public String toString() {
		return Boolean.toString(val);
	}

	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException {
		return new BoolValue(val);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException {
		return type = BoolType.singleton;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) {
		code.emit_bool(val);
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return BoolType.singleton;
	}
}
