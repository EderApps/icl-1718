package ast;

import types.IType;

public class Param {
	
	String x;
	IType type;
	
	public Param (String x, IType type) {
		this.x = x;
		this.type = type;
	}
	
	public String getParamX() {
		return x;
	}
	
	public IType getParamType() {
		return type;
	}
	
}
