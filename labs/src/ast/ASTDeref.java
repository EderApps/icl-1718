package ast;

import java.io.FileNotFoundException;

import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import types.BoolType;
import types.IType;
import types.IntType;
import types.RefType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.LookupEnv;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.RefValue;

public class ASTDeref implements ASTNode {
	ASTNode n;
	IType type;

	public ASTDeref(ASTNode n) {
		this.n = n;
	}
 
	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws UndeclaredIdentifierException, TypeMismatchException, DuplicateIdentifierException {
		RefValue val = ((RefValue)n.eval(env,m));
		return m.get(val);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		RefType t = (RefType) n.typecheck(env);
		return type = t.getType();
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		
		n.compile(code, env);
		
		code.emit_getfieldref(((RefType)(n.getType())));
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}
}