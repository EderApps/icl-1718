package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class ASTDisj implements ASTNode {

	ASTNode left, right;

	public ASTDisj(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " || " + right.toString();
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException, UndeclaredIdentifierException  {
		IValue l = left.eval(env,m);
		IValue r = right.eval(env,m);
		BoolValue lb = (BoolValue)l;
		BoolValue rb = (BoolValue)r;

		return new BoolValue(lb.getValue() || rb.getValue());
		
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException, UndeclaredIdentifierException  {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if(l == r) {
			
			return type = BoolType.singleton;
		}else {
			throw new TypingException();
		}
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env)  throws DuplicateIdentifierException{
		String labelFalse, labelFalse2, labelExit;
		
		labelFalse = code.labelFactory.getLabel();
		labelFalse2 = code.labelFactory.getLabel();
		labelExit = code.labelFactory.getLabel();
		
		left.compile(code,env);
		
		code.emit_ifeq(labelFalse);
		
		code.emit_bool(true);

		code.emit_jump(labelExit);

		code.emit_anchor(labelFalse);
		
		right.compile(code,env);
		
		code.emit_ifeq(labelFalse2);
		
		code.emit_bool(true);

		code.emit_jump(labelExit);		
		
		code.emit_anchor(labelFalse2);

		code.emit_bool(false);
		
		code.emit_anchor(labelExit);
				
	}

	IType type;
	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}

}
