package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.Environment;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTSeq implements ASTNode {

	ASTNode left, right;

	public ASTSeq(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " ; " + right.toString();
	}

	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException, UndeclaredIdentifierException  {
		left.eval(env,m);
		IValue r = right.eval(env,m);
		
		return r;
		
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException, UndeclaredIdentifierException {
		left.typecheck(env);
		IType r = right.typecheck(env);
		return type = r;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		left.compile(code, env);
		code.emit_pop();
		right.compile(code, env);
	}
	
	IType type;

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}
}
