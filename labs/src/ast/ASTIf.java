package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.Environment;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class ASTIf implements ASTNode {

	ASTNode left, right, inelse;

	public ASTIf(ASTNode n1, ASTNode n2, ASTNode n3) {
		left = n1;
		right = n2;
		inelse = n3;
	}

	@Override
	public String toString() {
		return "if "+left.toString() + " then " +right.toString() + " else " + inelse.toString();
	}

	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException, UndeclaredIdentifierException  {
		IValue l = left.eval(env,m);
		boolean lb = ((BoolValue)l).getValue();

		if(lb == true) {
			return right.eval(env,m);
		}else {
			return inelse.eval(env,m);
		}
		
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException, UndeclaredIdentifierException {
		IType in = inelse.typecheck(env);
		IType r = right.typecheck(env);
		IType l = left.typecheck(env);
		
		if (l == BoolType.singleton) {
		
		if(in.equals(r)) {
			return type = in;
		}else {
			throw new TypingException();
		}
		
		}else {
			throw new TypingException();
		}
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		String labelFalse, labelExit;
		
		labelFalse = code.labelFactory.getLabel();
		labelExit = code.labelFactory.getLabel();
		
		left.compile(code, env);
		
		code.emit_ifeq(labelFalse);
		
		right.compile(code, env);
		
		code.emit_jump(labelExit);
		
		code.emit_anchor(labelFalse);
		
		inelse.compile(code, env);
		
		code.emit_anchor(labelExit);
		
	}
	
	IType type;
	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}
}
