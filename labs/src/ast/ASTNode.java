package ast;

import values.IValue;
import compiler.CodeBlock;
import types.IType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;

public interface ASTNode {
	
	IValue eval(IEnvironment<IValue> env,MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException, UndeclaredIdentifierException;
	
	IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException, UndeclaredIdentifierException;
	
	void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException;
    
	IType getType();
}

