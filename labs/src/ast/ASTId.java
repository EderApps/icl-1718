package ast;

import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import types.IType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.LookupEnv;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTId implements ASTNode {
	String id;
	
	public ASTId(String id) {
		this.id = id;
	}
 
	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws UndeclaredIdentifierException {
		IEnvironment<IValue> newenv = env.beginScope();
		IValue val = null;
		val = env.find(id);
		env = newenv.endScope();
		return val;
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException {
		IEnvironment<IType> newenv = env.beginScope();
		IType val = null;
		val = env.find(id);		
		env = newenv.endScope();
		return type = val;
	}
	IType type;
	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		
		StackFrame curr = code.getCurrFrame();
		// Get the stack pointer
				/*
				aload SP
				checkcast frame_id
				*/
				if (curr != null) {
					code.emit_aload(0);
					code.emit_checkcast(curr.getID());
				}
				// ask code for the current frame
		
				// ask env for the address (jumps, loc_X)
				
				try {
					env.find(id);
					Address addr = env.lookup(id, curr.getID());
					int startID = curr.getID();
					for(int i = 0; i < addr.getJumps(); i++) {
						code.emit_getfieldframe(startID, --startID);
					}
					
					int i = addr.getLoc();
					String num = "00";
					if (i > 9) {
						num = ""+i;
					}else {
						num = "0"+i;
					}
					
					
					code.emit_getfieldloc(startID, num,curr.locs.get(i));
				
				} catch (UndeclaredIdentifierException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				// class Address { int jumps; int loc; ... }
				// Address addr = env.lookup(id) <<< Lookup in a class that extends Environment 
				// for each jump:
					/*
					getfield frame_n/SL Lframe_n1;
					getfield frame_n2/SL Lframe_n2;
					getfield frame_n2/SL Lframe_n3;
					…
					*/
				/*
				getfield frame_1/loc_X I ; <<<< I is the type of the identifier
		
				 */
				// use getType to know the field type
		
		
		
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}
}