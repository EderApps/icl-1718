package ast;

import java.util.ArrayList;

import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import types.IType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTDecl implements ASTNode {

	public static class Declaration {
		public Declaration(String id, ASTNode def) {
			this.id = id;
			this.def = def;
		}
		String id;
		public ASTNode def;
	}

	ASTNode body;
	ArrayList<Declaration> decls;

	public ASTDecl() {
		decls = new ArrayList<>();
	}

	public void addBody(ASTNode body) {
		this.body = body;
	}

	public void newBinding(String id, ASTNode e) {
		decls.add(new Declaration(id,e));
	}

	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException, UndeclaredIdentifierException {
		IEnvironment<IValue> newenv = env.beginScope();
		for(Declaration decl: decls) {
			newenv.assoc(decl.id, decl.def.eval(env,m));
		}
		IValue val = body.eval(newenv,m);
		env = newenv.endScope();
		return val;
	}
	IType type;
	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException, UndeclaredIdentifierException {
		IEnvironment<IType> newenv = env.beginScope();
		for(Declaration decl: decls) {
				newenv.assoc(decl.id, decl.def.typecheck(env));
		}
		IType val = body.typecheck(newenv);
		env = newenv.endScope();
		return type = val;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException { 
		
		// Create and initialize the stackframe (frame_i) in the compiler
				// Use code to manage stackframes ( code.newFrame() ??)
				// Generate code that initializes the stack frame

				/*
				new frame_id
				dup
				invokespecial frame_id/<init>()V
				*/
				System.out.println("envlevel = "+code.getEnvLevel());
				code.new_frame(decls);
				
				StackFrame currFrame = code.getCurrFrame();
				StackFrame upFrame = currFrame.getUp();
				
				code.emit_frameid(currFrame.getID());
				code.emit_dup();
				code.emit_invokespecialframe(currFrame.getID());
				
				// initialize SL in the stackframe
				/*
				dup
				aload SP
				putfield frame_id/SL Lframe_up; 
				*/
				
				if (upFrame != null && currFrame.getEnv() != 0) {
					code.emit_dup();
					code.emit_aload(0);
					code.emit_putfieldframe(currFrame.getID(), upFrame.getID());
				}
				
				
				// beginScope
				// For each declaration:
				int i = 0;
				String num = "00";
				IEnvironment<Address> newenv = env.beginScope();
				for(Declaration decl: decls) {
					// assoc id_i to loc_i (fresh)
					// add loc_i to frame (use the type)
					// compile its expression and generate code that stores the value
					// in the original environment
					/*
					  dup
					  [[ E_i ]] ei.compile()
					  putfield frame_id/loc_i type;
					*/

					newenv.assoc(decl.id, new Address(currFrame.getID(), i));
						
					code.emit_dup();
					decl.def.compile(code, newenv);
					
					if (i > 9) {
						num = ""+i;
					}else {
						num = "0"+i;
					}
					code.emit_putfieldloc(currFrame.getID(), num, decl);

					i++;
						
				}
				// terminate by storing the stack pointer
				/*
				astore SP
				*/
				code.emit_astore(0);
				
				// For the main declaration body
				// compiles it, in an environment that "knows" that loc_i corresponds to id_i
				/*
				[[ E ]] 
				*/
				code.incEnvLevel();

				body.compile(code, newenv);
				
				env = newenv.endScope();
				code.decEnvLevel();
				code.setCurrFrame(upFrame);
				// this corresponds to the endScope
				/*
				aload SP
				checkcast frame_id
				getfield frame_id/SL Lframe_up;
				astore SP
				 */
				
				//System.out.println("currID bef: "+currFrame.getID());
				//currFrame = code.getCurrFrame();
				//upFrame = currFrame.getUp();
				//System.out.println("currID aft: "+currFrame.getID());
				//System.out.println("upID aft: "+upFrame.getID());

				
				
				//code.emit_aload(0);
				//code.emit_checkcast(currFrame.getID());
				//code.emit_getfieldframe(currFrame.getID(), upFrame.getID());
				code.emit_aconstnull();
				code.emit_astore(0);
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}
}