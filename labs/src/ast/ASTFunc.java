package ast;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import compiler.CodeBlock;
import compiler.CodeBlock.ClosureClass;
import compiler.CodeBlock.FunInterface;
import compiler.CodeBlock.StackFrame;
import types.FuncType;
import types.IType;
import types.IntType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.Environment;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.Closure;
import values.IValue;
import values.IntValue;

public class ASTFunc implements ASTNode {

	IType type;
	
	ASTNode body;
	
	LinkedList<Param> params;
	
	public ASTFunc() {
		params = new LinkedList<Param>();
	}
	

	@Override
	public String toString() {
		return "fun";
	}

	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException, UndeclaredIdentifierException  {
		return new Closure(params, body, env);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException, UndeclaredIdentifierException {
		
		IEnvironment<IType> envLocal = env.beginScope();
		LinkedList<IType> parTypes = new LinkedList<IType>();
		for(Param p: params) {
			envLocal.assoc(p.getParamX(), p.getParamType());
			parTypes.add(p.getParamType());
		}
		
		
		IType t1 = body.typecheck(envLocal);
		if (t1 != null) {
			return type = new FuncType(t1, parTypes);
		}else {
			throw new TypingException();
		}
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		FuncType type = (FuncType)this.getType(); // from the typechecking phase
		LinkedList<IType> parTypes = type.getParTypes();
		IType retType = type.getRetType();		
		
		code.new_frame(null);
		
		StackFrame currFrame = code.getCurrFrame();
		StackFrame upFrame = currFrame.getUp();
		
		code.emit_frameid(currFrame.getID());
		code.emit_dup();
		code.emit_invokespecialframe(currFrame.getID());
		
		if (upFrame != null && currFrame.getEnv() != 0) {
			code.emit_dup();
			code.emit_aload(0);
			code.emit_putfieldframe(currFrame.getID(), upFrame.getID());
		}
		
		code.emit_astore(0);
		
		code.incEnvLevel();

				IEnvironment<Address> newenv = env.beginScope();

				int loc = 0;
				
				for(Param p: params) {
					newenv.assoc(p.getParamX(), new Address(loc++, loc++));
					code.new_frame(null);
					
					currFrame = code.getCurrFrame();
					upFrame = currFrame.getUp();
					System.out.println(upFrame);
					code.emit_frameid(currFrame.getID());
					code.emit_dup();
					code.emit_invokespecialframe(currFrame.getID());
					
					if (upFrame != null && currFrame.getEnv() != 0) {
						code.emit_dup();
						code.emit_aload(0);
						code.emit_putfieldframe(currFrame.getID(), upFrame.getID());
					}
					
					code.emit_astore(0);
				}
				FunInterface closureSignature = code.createOrReuse(type);
				ClosureClass closureCode = code.createClosure(closureSignature,params,newenv);

				code.emit_newClosure(closureCode); 

				code.emit_checkcast(currFrame.getID());
				code.emit_getfieldframe(currFrame.getID(), upFrame.getID());
				code.emit_astore(0);
				
		
	}

	@Override
	public IType getType() {
		return type;
	}

	public void setBody(ASTNode e2) {
		this.body = e2;
	}

	public void addArgument(String image, IType t) {
		params.add(new Param(image,t));
	}
}
