package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.RefType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.Environment;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;
import values.RefValue;

public class ASTAssign implements ASTNode {

	ASTNode left, right;
	IType type;

	public ASTAssign(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " ; " + right.toString();
	}

	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException, UndeclaredIdentifierException  {
		RefValue l = ((RefValue)left.eval(env, m));
		IValue r = right.eval(env, m);
		
		return m.set(l, r);
	
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException, UndeclaredIdentifierException {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if(r == ((RefType)(l)).getType()) {
			return type = r;
		}else {
			throw new TypingException();
		}
		
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		IType t = right.getType();
		left.compile(code, env);
		code.emit_dup();
		right.compile(code, env);
		code.emit_putfieldassign(left.getType(),t);
		code.emit_getfieldref(left.getType());
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}
}
