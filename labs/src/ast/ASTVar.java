package ast;

import java.io.FileNotFoundException;

import compiler.CodeBlock;
import compiler.CodeBlock.RefClass;
import compiler.CodeBlock.StackFrame;
import types.BoolType;
import types.IType;
import types.IntType;
import types.RefType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.LookupEnv;
import util.MemoryCell;
import util.MemoryImpl;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTVar implements ASTNode {
	ASTNode n;
	IType type;
	public ASTVar(ASTNode n) {
		this.n = n;
	}
 
	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws UndeclaredIdentifierException, TypeMismatchException, DuplicateIdentifierException {
		IValue val = n.eval(env,m);
		return m.create(val);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType val = n.typecheck(env);
		
		return type = new RefType(val);
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		
		try {
			code.new_ref(type);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
				
		code.emit_newref(type);
				
		code.emit_dup();
		
		code.emit_invokespecialRef(type);
		
		code.emit_dup();
		
		n.compile(code, env);
		
		code.emit_putfieldref(type);
		
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}
}