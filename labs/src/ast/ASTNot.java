package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class ASTNot implements ASTNode {

	ASTNode left;
	IType type;

	public ASTNot(ASTNode l) {
		left = l;
	}

	@Override
	public String toString() {
		return "!"+left.toString();
	}
	
	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException , UndeclaredIdentifierException {
		IValue l = left.eval(env,m);
		BoolValue lb = (BoolValue)l;

		return new BoolValue(!lb.getValue());
		
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException , UndeclaredIdentifierException {
		IType l = left.typecheck(env);
		
		return type = BoolType.singleton;
		
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		String labelTrue, labelExit;
		
		labelTrue = code.labelFactory.getLabel();
		labelExit = code.labelFactory.getLabel();

		left.compile(code,env);
		
		code.emit_ifeq(labelTrue);
		
		code.emit_bool(false);
		
		code.emit_jump(labelExit);
		
		code.emit_anchor(labelTrue);
		
		code.emit_bool(true);
		
		code.emit_anchor(labelExit);
		
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}

}
