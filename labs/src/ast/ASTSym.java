package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTSym implements ASTNode {

	ASTNode left;

	public ASTSym(ASTNode l) {
		left = l;
	}

	@Override
	public String toString() {
		return "- "+left.toString();
	}

	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException , UndeclaredIdentifierException {
		IValue l = left.eval(env,m);
		
		if(l instanceof IntValue ) {
			
			return new IntValue(-((IntValue)l).getValue());
		}else {
			throw new TypeMismatchException();
		}
		
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException , UndeclaredIdentifierException {
		return type = IntType.singleton;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env)  throws DuplicateIdentifierException{

		left.compile(code,env);
		
		code.emit_ineg();
		
	}
	IType type;

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}
}
