package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
import util.Address;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.MemoryManagement;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTDiv implements ASTNode {

	ASTNode left, right;
	IType type;
	public ASTDiv(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " / " + right.toString();
	}

	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException, DuplicateIdentifierException  , UndeclaredIdentifierException{
		IValue l = left.eval(env,m);
		IValue r = right.eval(env,m);
		
		//if(l instanceof IntValue && r instanceof IntValue) { // already checked the variables types in typecheck
			
			return new IntValue(((IntValue)l).getValue() / ((IntValue)r).getValue());
		/*}else {
			throw new TypeMismatchException();
		}*/
		
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, DuplicateIdentifierException , UndeclaredIdentifierException {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if(l == IntType.singleton && r == IntType.singleton) {
			
			return type = IntType.singleton;
		}else {
			throw new TypingException();
		}
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) throws DuplicateIdentifierException {
		left.compile(code,env);
		right.compile(code,env);
		code.emit_div();		
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return type;
	}
}
