package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
import util.Address;
import util.IEnvironment;
import util.MemoryManagement;
import values.IValue;
import values.IntValue;

public class ASTNum implements ASTNode {

	int val;
	IType type;
	
	public ASTNum(int n) {
		val = n;
	}

	@Override
	public String toString() {
		return Integer.toString(val);
	}

	@Override
	public IValue eval(IEnvironment<IValue> env, MemoryManagement m) throws TypeMismatchException {
		return new IntValue(val);
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException {
		return type = IntType.singleton;
	}

	@Override
	public void compile(CodeBlock code, IEnvironment<Address> env) {
		code.emit_push(val);
		
	}

	@Override
	public IType getType() {
		// TODO Auto-generated method stub
		return IntType.singleton;
	}
}
